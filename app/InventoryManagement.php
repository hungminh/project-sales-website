<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryManagement extends Model
{
    protected $table = 'inventory_managements';

    use SoftDeletes;
}
