<?php

namespace App\Http\Controllers\Supplier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Supplier;

class SupplierController extends Controller
{

    public function list()
    {
        $supplier = Supplier::all();
        return view('supplier/list-supplier',compact('supplier'));
    }

    public function create()
    {
        return view('supplier/create-supplier');
    }

    public function store(Request $request)
    {
        $createSupplier = new Supplier;
        $createSupplier->name = $request->suppliername;
        $createSupplier->description = $request->description;
        $createSupplier->phone_number = $request->hotline;
        $createSupplier->address = $request->address;

        $createSupplier->save();
        return redirect('admin/supplier')->with('success','Save Category Successfully');
    }

    public function edit($id)
    {
        $detailSupplier = Supplier::find($id);
        return view('supplier/edit-supplier',compact('detailSupplier'));
    }

    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);
        $supplier->name = $request->suppliername;
        $supplier->description = $request->description;
        $supplier->phone_number = $request->hotline;
        $supplier->address = $request->address;

        $supplier->save();
        return redirect('admin/supplier');

    }

    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        $supplier->delete();

        return redirect('admin/supplier');
    }
}
