<?php

namespace App\Http\Controllers\Warehouse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WarehouseManagement;

class WarehouseManagementController extends Controller
{
    public function list()
    {
        $warehouse = WarehouseManagement::all();
        return view('warehouse/list-product-warehouse', compact('warehouse'));
    }

    public function detail($id)
    {
        $detailWarehouse = WarehouseManagement::find($id);
        return view('warehouse/detail-product-warehouse', compact('detailWarehouse'));
    }
}
