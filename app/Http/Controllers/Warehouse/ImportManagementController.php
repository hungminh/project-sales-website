<?php

namespace App\Http\Controllers\Warehouse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ImportManagement;

class ImportManagementController extends Controller
{
    public function list()
    {
        $import = ImportManagement::all();
        return view('warehouse/import/list-import-product', compact('import'));
    }

    public function create()
    {
        return view('warehouse/import/create-import-product');
    }

    public function store(Request $request)
    {
        $total = ((int)$request->price * (int)$request->quantity);

        $createImport = new ImportManagement();
        
        $createImport->code = $request->code;
        $createImport->product_name = $request->name;
        $createImport->supplier = $request->supplier;
        $createImport->price = $request->price;
        $createImport->quantity = $request->quantity;
        $createImport->amount = $total;

        $createImport->save();

        return redirect('admin/import');
    }

    public function edit($id)
    {
        $detailImport = ImportManagement::find($id);
        return view('warehouse/import/detail-import-product', compact('detailImport'));
    }

    public function update(Request $request, $id)
    {
        $total = ((int)$request->price * (int)$request->quantity);

        $update = ImportManagement::find($id);
        
        $update->code = $request->code;
        $update->product_name = $request->name;
        $update->supplier = $request->supplier;
        $update->price = $request->price;
        $update->quantity = $request->quantity;
        $update->amount = $total;

        $update->save();

        return redirect('admin/import');
    }
}
