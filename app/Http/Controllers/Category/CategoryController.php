<?php

namespace App\Http\Controllers\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{

    public function list()
    {
        $category = Category::all();
        return view('category/list-category',compact('category'));
    }

    public function create()
    {
        return view('category/create-category');
    }

    public function store(Request $request)
    {
        $createNewCategory = new Category;
        $createNewCategory->user_id = 1;
        $createNewCategory->name = $request->category;
        $createNewCategory->description = $request->description;
        if(isset($request->is_published)) 
        {
            $createNewCategory->is_published = 1;
        }
        else {
            $createNewCategory->is_published = 0;
        }

        $createNewCategory->save();
        return redirect('admin/category')->with('success','Save Category Successfully');
    }

    public function edit($id)
    {
        $detailOfCategory = Category::find($id);
        return view('category/edit-category',compact('detailOfCategory'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->user_id = 1;
        $category->name = $request->name;
        $category->description = $request->description;
        if(isset($request->is_published)) 
        {
            $category->is_published = 1;
        }
        else {
            $category->is_published = 0;
        }
        
        $category->save();
        return redirect('admin/category');

    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect('admin/category');
    }

}
