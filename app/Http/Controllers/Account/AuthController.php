<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use App\Account;


class AuthController extends Controller
{



    public function authLogin(Request $request)
    {
        if(Auth::guard('users')->attempt(['email'=>$request->email, 'password'=>$request->password]))
        {
            session(['email'=>$request->email]);
            return redirect('admin/dashboard');
        }
        else
        {
            return view('auth/login')->with('error','Incorrect username or password.');
        }
    }
}
