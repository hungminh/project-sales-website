<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Supplier;
use App\Category;

class ProductController extends Controller
{
    
    public function list()
    {
        $product = Product::all();
        return view('myproduct/product/list-product', compact('product'));
    }

    public function create()
    {
        return view('myproduct/product/create-product');
    }

    public function detail($id)
    {
        $detailProduct = Product::find($id);
        $supplier = Supplier::all();
        $category = Category::all();
        return view('myproduct/product/edit-product', compact('detailProduct','category','supplier'));
    }

    public function update(Request $request, $id)
    {
        
    }

}
