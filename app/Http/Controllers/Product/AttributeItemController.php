<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductAttributeItem;
use App\ProductAttribute;

class AttributeItemController extends Controller
{
    public function list()
    {
        $attributeItem = ProductAttributeItem::all();
        return view('myproduct/attribute/item-attribute/list-attribute-item',compact('attributeItem'));
    }

    public function create()
    {
        $attribute = ProductAttribute::all();
        return view('myproduct/attribute/item-attribute/create-attribute-item', compact('attribute'));
    }

    public function store(Request $request)
    {
        $slug = str_slug($request->attributeItem, '-');

        $createAttributeItem = new ProductAttributeItem;
        $createAttributeItem->attribute_id = $request->attribute;
        $createAttributeItem->name = $request->attributeItem;
        $createAttributeItem->slug = $slug;

        $createAttributeItem->save();
        return redirect('admin/attribute-item')->with('success','Save Category Successfully');
    }

    public function edit($id)
    {
        $attribute = ProductAttribute::all();
        $detailAttributeItem = ProductAttributeItem::find($id);
        return view('myproduct/attribute/item-attribute/edit-attribute-item',compact('detailAttributeItem', 'attribute'));
    }

    public function update(Request $request, $id)
    {
        $slug = str_slug($request->attributeItem, '-');

        $attribute = ProductAttributeItem::find($id);
        $attribute->attribute_id = $request->attribute;
        $attribute->name = $request->attributeItem;
        $attribute->slug = $slug;

        $attribute->save();
        return redirect('admin/attribute-item');

    }

    public function destroy($id)
    {
        $attribute = ProductAttributeItem::find($id);
        $attribute->delete();

        return redirect('admin/attribute-item');
    }
}
