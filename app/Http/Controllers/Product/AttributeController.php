<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductAttribute;

class AttributeController extends Controller
{
    public function list()
    {
        $attribute = ProductAttribute::all();
        return view('myproduct/attribute/list-attribute',compact('attribute'));
    }

    public function create()
    {
        return view('myproduct/attribute/create-attribute');
    }

    public function store(Request $request)
    {
        $slug = str_slug($request->attribute, '-');
        
        $createAttribute = new ProductAttribute;
        $createAttribute->name = $request->attribute;
        $createAttribute->slug = $slug;

        $createAttribute->save();
        return redirect('admin/attribute')->with('success','Save Category Successfully');
    }

    public function edit($id)
    {
        $detailAttribute = ProductAttribute::find($id);
        return view('myproduct/attribute/edit-attribute',compact('detailAttribute'));
    }

    public function update(Request $request, $id)
    {
        $slug = str_slug($request->attribute, '-');

        $attribute = ProductAttribute::find($id);
        $attribute->name = $request->attribute;
        $attribute->slug = $slug;

        $attribute->save();
        return redirect('admin/attribute');

    }

    public function destroy($id)
    {
        $attribute = ProductAttribute::find($id);
        $attribute->delete();

        return redirect('admin/attribute');
    }
}
