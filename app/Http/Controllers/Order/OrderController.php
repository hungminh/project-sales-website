<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;

class OrderController extends Controller
{
    public function list()
    {
        $order = Order::all();
        return view('order/list-order');
    }

    public function detail($id)
    {
        $detailOfCategory = Category::find($id);
        return view('order/detail-order',compact('detailOfCategory'));
    }

}
