<?php

namespace App\Http\Controllers\Slider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;
use App\SliderImage;

class SliderController extends Controller
{
    public function list()
    {
        $sliders = Slider::all();
        return view('slider/list-slider',compact('sliders'));
    }
    public function create()
    {
        return view('slider/create-slider');
    }

    public function uploadImage($image, $slider_id)
    {
        $createSliderImage = new SliderImage;
        if(isset($image)) {
            $file = $image;
            $filename = $slider_id.$file->getClientOriginalName();
            $file->move(public_path('image'),$filename);
            $createSliderImage->slider_id = $slider_id;
            $createSliderImage->image = $filename;
            $createSliderImage->save();
        }
    }

    public function store(Request $request)
    {
        $createSlider = new Slider;
        $createSlider->caption = $request->caption;
        if(isset($request->status)) 
        {
            $createSlider->status = 1;
        }
        else {
            $createSlider->status = 0;
        }
        $createSlider->save();
        $this->uploadImage($request->slider_image_1, $createSlider->id);
        $this->uploadImage($request->slider_image_2, $createSlider->id);
        $this->uploadImage($request->slider_image_3, $createSlider->id);
        $this->uploadImage($request->slider_image_4, $createSlider->id);
        return redirect('admin/slider')->with('success','Save Slider Successfully');
    }

    public function edit($id)
    {
        $detailSlider = Slider::find($id);
        $sliderImage = SliderImage::where('slider_id', $id)->get();
        return view('slider/detail-slider',compact('detailSlider', 'sliderImage'));
    }
    public function update(Request $request, $id)
    {
        $slider = Slider::find($id);
        $slider->caption = $request->caption;
        if(isset($request->status)) 
        {
            $slider->status = 1;
        }
        else {
            $slider->status = 0;
        }
        $slider->save();
        $this->uploadImage($request->slider_image_1, $slider->id);
        $this->uploadImage($request->slider_image_2, $slider->id);
        $this->uploadImage($request->slider_image_3, $slider->id);
        $this->uploadImage($request->slider_image_4, $slider->id);
        return redirect('admin/slider');
    }
    public function destroy($id)
    {
        $attribute = Slider::find($id);
        $attribute->delete();
        return redirect('admin/slider');
    }
}
