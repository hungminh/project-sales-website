<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductAttributeItem extends Model
{
    protected $table = 'product_attribute_items';

    use SoftDeletes;
}
