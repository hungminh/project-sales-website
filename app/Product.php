<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $table = 'products';

    use SoftDeletes;

    const PRODUCT_ACTIVE = [
        'normal' => 'Normal',
        'sell' => 'Sell',
        'bestseller' => 'Best Seller',
        'soldout' => 'Sold Out',
    ];
}
