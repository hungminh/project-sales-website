<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $table = 'categories';

    use SoftDeletes;

    public static function getIsPublishedText($value)
    {
        if($value == 1)
        {
            return "Đã công khai";
        }
        return "Chưa công khai";
    }
}
