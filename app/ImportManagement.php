<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImportManagement extends Model
{
    protected $table = 'import_managements';

    use SoftDeletes;
}
