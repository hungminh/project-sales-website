<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseManagement extends Model
{
    protected $table='warehouse_managements';
    
    use SoftDeletes;
}
