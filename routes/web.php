<?php

// Route::middleware('authAdmin')->group(function() {

// });

//Login Admin
Route::get('/admin/login', function(){
    return view('auth/login');
});

Route::post('/admin/login', 'Account\AuthController@authLogin')->name('login');

// Page admin
Route::prefix('admin')->group(function() {

        
        // Auth View Profile
        Route::get('/profile', 'Account\AuthController@getInfoProfile')->name('profile');
        Route::post('/update-profile', 'Account\AuthController@updateProfile')->name('updateProfile');

        //Change Password
        Route::post('change-password','Account\AuthController@changePassword')->name('changePassword');


        // User
        Route::prefix('user')->group(function() {
            Route::name('user.')->group(function() {
                    Route::get('/','Account\AccountController@listAccount')->name('list');
                    Route::post('/{id}','Account\AccountController@updateUserToStaff')->name('Staff');
            });

        });


         // Product
        Route::prefix('product')->group(function() {


             //Product
             Route::name('product.')->group(function() {
                Route::get('/', 'Product\ProductController@list')->name('list');
                Route::get('/create', 'Product\ProductController@create')->name('created');
                Route::post('/create', 'Product\ProductController@store')->name('stored');
                Route::get('/{id}', 'Product\ProductController@edit')->name('detail');
                Route::post('/{id}/update', 'Product\ProductController@update')->name('updated');
                Route::get('/{id}/deleted', 'Product\ProductController@destroy')->name('deleted');
                });


                //Variant Product
                Route::name('variant.')->group(function() {
                    Route::get('{product_id}/product-variant','Product\ProductVariantController@listProductVariant')
                            ->name('list');
                    Route::get('{product_id}/product-variant', 'Product\ProductVariantController@createProductVariant')
                            ->name('getPageCreate');
                    Route::post('{product_id}/product-variant', 'Product\ProductVariantController@createProductVariant')
                            ->name('created');
                    Route::get('{product_id}/product-variant/{id}', 'Product\ProductVariantController@detailProductVariant')
                            ->name('detail');
                    Route::post('{product_id}/product-variant/{id}', 'Product\ProductVariantController@updateProductVariant')
                            ->name('updated');
                    Route::get('{product_id}/product-variant/{id}', 'Product\ProductVariantController@deleteProductVariant')
                            ->name('deleted');
                });

        
                //Image Product
                Route::name('productimage.')->group(function() {
                    Route::get('{product_id}/product-image','Product\ProductImageController@listProductImage')
                            ->name('listImage');
                    Route::get('{product_id}/product-image/{id}','Product\ProductImageController@detailProductImage')
                            ->name('detail');
                    Route::post('{product_id}/product-image','Product\ProductImageController@uploadImageOfProduct')
                            ->name('uploaded');
                    Route::post('{product_id}/product-image{id}','Product\ProductImageController@updateImageOfProduct')
                            ->name('updated');
                    Route::get('{product_id}/product-image{id}','Product\ProductImageController@deleteImageOfProduct')
                            ->name('deleted');
                });
            
        });

        //Category
        Route::prefix('category')->group(function() {
            Route::name('category.')->group(function() {
                Route::get('/', 'Category\CategoryController@list')->name('list');
                Route::get('/create', 'Category\CategoryController@create')->name('created');
                Route::post('/create', 'Category\CategoryController@store')->name('stored');
                Route::get('/{id}', 'Category\CategoryController@edit')->name('detail');
                Route::post('/{id}/update', 'Category\CategoryController@update')->name('updated');
                Route::get('/{id}/deleted', 'Category\CategoryController@destroy')->name('deleted');
            });

        });


        //Attribute
        Route::prefix('attribute')->group(function() {
            Route::name('attribute.')->group(function() {
                    Route::get('/','Product\AttributeController@list')
                            ->name('list');
                    Route::get('/created','Product\AttributeController@create')
                            ->name('created');
                    Route::post('/created','Product\AttributeController@store')
                            ->name('stored');
                    Route::get('/{id}','Product\AttributeController@edit')
                            ->name('edit');
                    Route::post('/{id}/update','Product\AttributeController@update')
                            ->name('updated');
                    Route::get('/{id}/deleted','Product\AttributeController@destroy')
                            ->name('deleted');
            });
        });

    
        //Attribute Item
        Route::prefix('attribute-item')->group(function() {
            Route::name('attributeitem.')->group(function() {
                    Route::get('/','Product\AttributeItemController@list')
                            ->name('list');
                    Route::get('/create','Product\AttributeItemController@create')
                            ->name('created');
                    Route::post('/create','Product\AttributeItemController@store')
                            ->name('stored');
                    Route::get('/{id}','Product\AttributeItemController@edit')
                            ->name('edit');
                    Route::post('/{id}/update','Product\AttributeItemController@update')
                            ->name('updated');
                    Route::get('/delete/{id}','Product\AttributeItemController@destroy')
                            ->name('deleted');
            });
        });


        // Page Supplier
        Route::prefix('supplier')->group(function() {

            Route::name('supplier.')->group(function() {
                    Route::get('/','Supplier\SupplierController@list')
                            ->name('list');
                    Route::get('/create','Supplier\SupplierController@create')
                            ->name('created');
                    Route::post('create','Supplier\SupplierController@store')
                            ->name('stored');
                    Route::get('/{id}','Supplier\SupplierController@edit')
                            ->name('edit');
                    Route::post('/{id}/update','Supplier\SupplierController@update')
                            ->name('updated');
                    Route::get('/{id}/deleted','Supplier\SupplierController@destroy')
                            ->name('deleted'); 
            });

        });


        // Page DashBoard
        Route::get('dashboard','DashBoardController@dashBoard')->name('dashboard');

        // Page Order 
        Route::prefix('order')->group(function() {

                //Order
                Route::name('order.')->group(function() {
                        Route::get('/','Order\OrderController@list')
                                ->name('list');
                        Route::get('/order-detail','Order\OrderController@detail')
                                ->name('detail'); 
                });

        });


        // Warehouse
        Route::prefix('warehouse')->group(function() {

            Route::name('warehouse.')->group(function() {
                    Route::get('/','Warehouse\WarehouseManagementController@list')
                            ->name('list');
                    Route::get('/{id}','Warehouse\WarehouseManagementController@detail')
                            ->name('detail');
                    Route::post('/{id}','Warehouse\WarehouseManagementController@update')
                            ->name('updated');
            });

        });


        // Import Management
        Route::prefix('import')->group(function() {

            Route::name('import.')->group(function() {
                    Route::get('/','Warehouse\ImportManagementController@list')
                            ->name('list');
                    Route::get('/create','Warehouse\ImportManagementController@create')
                            ->name('create');
                    Route::post('create','Warehouse\ImportManagementController@store')
                            ->name('stored');
                    Route::get('/{id}','Warehouse\ImportManagementController@edit')
                            ->name('edit');
                    Route::post('/{id}','Warehouse\ImportManagementController@update')
                            ->name('updated');
            });

        });


        // Inventory Management
        Route::prefix('inventory')->group(function() {

            Route::name('inventory.')->group(function() {
                    Route::get('/','Warehouse\InventoryManagementController@list')
                            ->name('list');
                    Route::get('/create','Warehouse\InventoryManagementController@create')
                            ->name('created');
                    Route::post('create','Warehouse\InventoryManagementController@store')
                            ->name('stored');
                    Route::get('/{id}','Warehouse\InventoryManagementController@edit')
                            ->name('edit');
                    Route::post('/{id}','Warehouse\InventoryManagementController@update')
                            ->name('updated');
            });

        });


});