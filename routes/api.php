<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('product')->group(function() {
    Route::get('/','Product\ProductController@getProductAPI');
});

Route::get('/category','Api\CategoryController@getCategories');