<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('code',16)->unique();
            $table->text('description');
            $table->integer('price');
            $table->integer('price_promo');
            $table->integer('status');
            $table->integer('quantity');
            $table->json('attribute');
            $table->boolean('is_published');
            $table->integer('category_id')->unsigned();
            $table->integer('supplier_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
