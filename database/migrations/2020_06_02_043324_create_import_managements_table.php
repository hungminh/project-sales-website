<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportManagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_managements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',16)->unique();
            $table->string('product_name',255);
            $table->string('supplier',255);
            $table->integer('price');
            $table->integer('quantity');
            $table->integer('amount');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_managements');
    }
}
