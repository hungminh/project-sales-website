@extends('supplier/layout-supplier')

<!-- Main content  -->
<?php $i=1 ?>
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">
            <div class="card card-plain">
                <div class="card-header card-header-primary">
                    <h4 class="card-title" style="display:inline">Nhà cung cấp</h4>
                    <a href="{{route('supplier.created')}}" style="float:right;color:white;">
                    Thêm mới
                    <i class="material-icons">add_circle</i>
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table dt-responsive nowrap">
                            <thead class=" text-primary">
                            <th>STT</th>
                            <th>Tên nhà cung cấp</th>
                            <th>Số điện thoại</th>
                            <th>Địa chỉ</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($supplier as $suppliers)
                                <tr>
                                    <td><?php echo $i++ ?></td>
                                    <td>{{$suppliers->name}}</td>
                                    <td>{{$suppliers->phone_number}}</td>
                                    <td>{{$suppliers->address}}</td>
                                    <td>
                                    <a href="{{route('supplier.edit', ['id' => $suppliers->id])}}">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a href="{{route('supplier.deleted', ['id' => $suppliers->id])}}">
                                        <i class="material-icons">delete</i>
                                    </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>      
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')

@section('js')
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<!-- jquery pagination list  -->
<script type="text/javascript">
    $(document).ready(function(){$("#datatable").DataTable({
    language:{
        paginate:{
        previous:"<i class='mdi mdi-chevron-left'>",next:"<i class='mdi mdi-chevron-right'>"
                }
            },drawCallback:function(){
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
    });
    });
</script>
@endsection('js')