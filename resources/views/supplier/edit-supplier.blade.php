@extends('supplier/layout-supplier')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Tạo mới nhà cung cấp</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form action="{{route('supplier.updated', ['id' => $detailSupplier->id])}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên nhà cung cấp</label>
                <input type="text" class="form-control" name="suppliername" id="suppliername" value="{{$detailSupplier->name}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Mô tả nhà cung cấp</label>
            <input type="textarea" class="form-control" name="description" id="description" value="{{$detailSupplier->description}}">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Hotline</label>
            <input type="textarea" class="form-control" name="hotline" id="hotline" value="{{$detailSupplier->phone_number}}">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Địa chỉ</label>
            <input type="textarea" class="form-control" name="address" id="address" value="{{$detailSupplier->address}}">
        </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Cập nhật</button>
</form>
@endsection('content')