@extends('supplier/layout-supplier')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Tạo mới nhà cung cấp</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form action="{{route('supplier.updated')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên nhà cung cấp</label>
                <input type="text" class="form-control" name="suppliername" id="suppliername">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Mô tả nhà cung cấp</label>
            <input type="textarea" class="form-control" name="description" id="description">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Hotline</label>
            <input type="textarea" class="form-control" name="hotline" id="hotline">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Địa chỉ</label>
            <input type="textarea" class="form-control" name="address" id="address">
        </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Tạo mới</button>
</form>
@endsection('content')