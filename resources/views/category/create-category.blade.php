@extends('category/layout-category')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Tạo mới loại sản phẩm</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form action="{{route('category.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên loại sản phẩm</label>
                <input type="text" class="form-control" name="category" id="category">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Mô tả loại sản phẩm</label>
            <input type="textarea" class="form-control" name="description" id="description">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Bạn có muốn hiển thị loại sản phẩm?</label>
                <input type="checkbox" name="is_published" id="is_published">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Tạo loại sản phẩm</button>
</form>
@endsection('content')