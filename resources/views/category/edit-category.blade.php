@extends('category/layout-category')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Cập nhật loại sản phẩm</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form action="{{route('category.updated',['id' => $detailOfCategory->id])}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên sản phẩm</label>
                <input type="text" class="form-control" name="name" id="name" value="{{$detailOfCategory->name}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Mô tả sản phẩm</label>
            <input type="textarea" class="form-control" name="description" id="description" value="{{$detailOfCategory->description}}">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Bạn có muốn hiển thị sản phẩm?</label>
                <input type="checkbox" name="is_published" id="is_published" @if($detailOfCategory->is_published==1) checked @endif>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Cập nhật loại sản phẩm</button>
</form>
@endsection('content')