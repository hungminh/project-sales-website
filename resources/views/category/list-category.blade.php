@extends('category/layout-category')

<!-- Main content  -->
<?php $i=1 ?>
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">
            <div class="card card-plain">
                <div class="card-header card-header-primary">
                    <h4 class="card-title" style="display:inline">Loại sản phẩm</h4>
                    <a href="{{route('category.created')}}" style="float:right;color:white;">
                    Thêm mới
                    <i class="material-icons">add_circle</i>
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table dt-responsive nowrap">
                            <thead class=" text-primary">
                            <th>STT</th>
                            <th>Người tạo</th>
                            <th>Tên sản phẩm</th>
                            <th>Mô tả sản phẩm</th>
                            <th>Công bố</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($category as $categories)
                                <tr>
                                    <td><?php echo $i++ ?></td>
                                    <td>{{$categories->user_id}}</td>
                                    <td>{{$categories->name}}</td>
                                    <td>{{$categories->description}}</td>
                                    <td>{{App\Category::getIsPublishedText($categories->is_published)}}</td>
                                    <td>
                                    <a href="{{route('category.detail', ['id' => $categories->id])}}">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a href="{{route('category.deleted', ['id' => $categories->id])}}">
                                        <i class="material-icons">delete</i>
                                    </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>      
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')

@section('js')
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<!-- jquery pagination list  -->
<script type="text/javascript">
    $(document).ready(function(){$("#datatable").DataTable({
    language:{
        paginate:{
        previous:"<i class='mdi mdi-chevron-left'>",next:"<i class='mdi mdi-chevron-right'>"
                }
            },drawCallback:function(){
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
    });
    });
</script>
@endsection('js')