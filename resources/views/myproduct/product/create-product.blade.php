@extends('myproduct/layout-product')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Tạo sản phẩm</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form method="POST" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="bmd-label-floating">Tên sản phẩm</label>
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="bmd-label-floating">Trạng thái sản phẩm</label>
                <input type="text" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="bmd-label-floating">Giá sản phẩm</label>
                <input type="text" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="bmd-label-floating">Giá khuyến mãi</label>
                <input type="text" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Loại sản phẩm</label>
                <input type="text" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Nhà cung cấp</label>
                <input type="text" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Mô tả sản phẩm</label>
            <input type="textarea" class="form-control">
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Bạn có muốn hiển thị sản phẩm?</label>
                <input type="checkbox">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Cập nhật loại sản phẩm</button>
</form>
@endsection('content')