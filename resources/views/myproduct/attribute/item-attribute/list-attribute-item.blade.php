@extends('myproduct/layout-product')

<!-- Main content  -->
<?php $i=1 ?>
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">
            <div class="card card-plain">
                <div class="card-header card-header-primary">
                    <h4 class="card-title" style="display:inline">Danh sách các giá trị con của thuộc tính</h4>
                    <a href="{{route('attributeitem.created')}}" style="float:right;color:white;">
                    Thêm mới
                    <i class="material-icons">add_circle</i>
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table dt-responsive nowrap">
                            <thead class=" text-primary">
                            <th>STT</th>
                            <th>Thuộc tính</th>
                            <th>Tên giá trị thuộc tính</th>
                            <th>Slug</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($attributeItem as $attributeItems)
                                <tr>
                                    <td><?php echo $i++ ?></td>
                                    <td>{{$attributeItems->attribute_id}}</td>
                                    <td>{{$attributeItems->name}}</td>
                                    <td>{{$attributeItems->slug}}</td>
                                    <td>
                                    <a href="{{route('attributeitem.edit', ['id' => $attributeItems->id])}}">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a href="{{route('attributeitem.deleted', ['id' => $attributeItems->id])}}">
                                        <i class="material-icons">delete</i>
                                    </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>      
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')

@section('js')
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<!-- jquery pagination list  -->
<script type="text/javascript">
    $(document).ready(function(){$("#datatable").DataTable({
    language:{
        paginate:{
        previous:"<i class='mdi mdi-chevron-left'>",next:"<i class='mdi mdi-chevron-right'>"
                }
            },drawCallback:function(){
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
    });
    });
</script>


@endsection('js')