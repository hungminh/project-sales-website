@extends('myproduct/layout-product')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Tạo mới giá thuộc tính sản phẩm</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form action="{{route('attributeitem.stored')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên thuộc tính</label>
                <a href="{{route('attribute.created')}}" style="float:right">Tạo mới thuộc tính</a>
                <select class="form-control" name="attribute" id="attribute">
                    <option value="">Lựa chọn thuộc tính</option>
                        @foreach($attribute as $attributes)
                            <option value="{{$attributes->id}}">{{$attributes->name}}</option>
                        @endforeach()
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên giá trị thuộc tính</label>
                <input type="text" class="form-control" name="attributeItem" id="attributeItem">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Tạo giá trị thuộc tính</button>
</form>
@endsection('content')