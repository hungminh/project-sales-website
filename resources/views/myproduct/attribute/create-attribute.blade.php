@extends('myproduct/layout-product')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Tạo mới thuộc tính sản phẩm</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form action="{{route('attribute.stored')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên thuộc tính</label>
                <input type="text" class="form-control" name="attribute" id="attribute">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Tạo thuộc tính</button>
</form>
@endsection('content')