@extends('warehouse/layout-warehouse')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Cập nhật kho</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form>
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Code sản phẩm</label>
                <input type="text" class="form-control" value="{{$detailWarehouse->code}}" disabled>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Tên sản phẩm</label>
            <input type="textarea" class="form-control" value="{{$detailWarehouse->product_name}}" disabled>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Tên nhà cung cấp</label>
            <input type="textarea" class="form-control" value="{{$detailWarehouse->supplier}}" disabled>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Giá mua vào</label>
            <input type="textarea" class="form-control" value="{{$detailWarehouse->purchase_price}}" disabled>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <div class="form-group">
            <label class="bmd-label-floating">Số lượng</label>
            <input type="textarea" class="form-control" value="{{$detailWarehouse->quantity}}" disabled>
        </div>
        </div>
    </div>
    <a href="{{route('warehouse.list')}}" style="float:right">
        <i class="material-icons">keyboard_return</i>
    </a>
</form>
@endsection('content')