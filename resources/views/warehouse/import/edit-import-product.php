@extends('warehouse/layout-warehouse')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Nhập hàng</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form action="#" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Mã sản phẩm</label>
                <input type="text" class="form-control" value="sản phẩm 1">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên sản phẩm</label>
                <input type="text" class="form-control" value="sản phẩm 1">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Nhà cung cấp</label>
                <input type="textarea" class="form-control" value="sản phẩm 1">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Giá mua vào</label>
                <input type="text" class="form-control" value="sản phẩm 1">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Số lượng</label>
                <input type="text" class="form-control" value="sản phẩm 1">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tổng tiền</label>
                <input type="text" class="form-control" value="sản phẩm 1">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Bạn có muốn hiển thị sản phẩm?</label>
                <input type="checkbox">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Cập nhật loại sản phẩm</button>
</form>
@endsection('content')