@extends('warehouse/layout-warehouse')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Nhập hàng</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form action="{{route('import.stored')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Mã sản phẩm</label>
                <input type="text" class="form-control" id="code" name="code">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên sản phẩm</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Nhà cung cấp</label>
                <input type="textarea" class="form-control" id="supplier" name="supplier">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Giá mua vào</label>
                <input type="text" class="form-control" id="price" name="price">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Số lượng</label>
                <input type="text" class="form-control" id="quantity" name="quantity">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Thêm mới</button>
</form>
@endsection('content')