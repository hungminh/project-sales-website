@extends('warehouse/layout-warehouse')

<!-- Main content  -->
<?php $i=1 ?>
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">
            <div class="card card-plain">
                <div class="card-header card-header-primary">
                    <h4 class="card-title" style="display:inline">Quản lý kho</h4>
                    <a href="{{route('import.create')}}" style="float:right;color:white;">
                    Nhập sản phẩm vào kho
                    <i class="material-icons">add_circle</i>
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table dt-responsive nowrap">
                            <thead class=" text-primary">
                            <th>STT</th>
                            <th>Code</th>
                            <th>Tên sản phẩm</th>
                            <th>Nhà cung cấp</th>
                            <th>Giá mua vào</th>
                            <th>Số lượng tồn kho</th>
                            <th>Tổng tiền</th>
                            <th>Ngày nhập</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($import as $imports)
                                <tr>
                                    <td><?php echo $i++ ?></td>
                                    <td>{{$imports->code}}</td>
                                    <td>{{$imports->product_name}}</td>
                                    <td>{{$imports->supplier}}</td>
                                    <td>{{$imports->price}}</td>
                                    <td>{{$imports->quantity}}</td>
                                    <th>{{$imports->amount}}</th>
                                    <th>{{$imports->created_at}}</th>
                                    <td>
                                    <a href="{{route('import.edit', ['id' => $imports->id])}}">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>      
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')

@section('js')
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<!-- jquery pagination list  -->
<script type="text/javascript">
    $(document).ready(function(){$("#datatable").DataTable({
    language:{
        paginate:{
        previous:"<i class='mdi mdi-chevron-left'>",next:"<i class='mdi mdi-chevron-right'>"
                }
            },drawCallback:function(){
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
    });
    });
</script>
@endsection('js')