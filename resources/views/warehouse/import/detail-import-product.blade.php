@extends('myproduct/layout-product')

@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Nhập hàng</a>
</div>
@endsection('title')

@section('content')
<!-- Main content  -->
<form action="{{route('import.updated', ['id' => $detailImport->id])}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Mã sản phẩm</label>
                <input type="text" class="form-control" id="code" name="code" value="{{$detailImport->code}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Tên sản phẩm</label>
                <input type="text" class="form-control" id="name" name="name" value="{{$detailImport->product_name}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Nhà cung cấp</label>
                <input type="textarea" class="form-control" id="supplier" name="supplier" value="{{$detailImport->supplier}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Giá mua vào</label>
                <input type="text" class="form-control" id="price" name="price" value="{{$detailImport->price}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="bmd-label-floating">Số lượng</label>
                <input type="text" class="form-control" id="quantity" name="quantity" value="{{$detailImport->quantity}}">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Tiếp tục nhập hàng</button>
</form>
@endsection('content')