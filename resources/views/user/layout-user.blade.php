@extends('layout')

<!-- Title of Table -->
@section('title')
<div class="navbar-wrapper">
    <a class="navbar-brand" href="#">Quản lý tài khoản</a>
</div>
@endsection('title')

<!-- Layout Menu -->
@section('menu')
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item  ">
          <a class="nav-link" href="./dashboard.html">
            <i class="material-icons">dashboard</i>
            <p>Quản lý</p>
          </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="./user.html">
            <i class="material-icons">person</i>
            <p>Tài khoản</p>
          </a>
        </li>
        <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown" data-toggle="dropdown">
                <i class="material-icons">list</i>
                <span class="caret"></span>
                <p>Sản phẩm</p>
            </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Danh sách sản phẩm</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Thuộc tính sản phẩm</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Giá trị thuộc tính</a></li>
                </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./tables.html">
            <i class="material-icons">store</i>
            <p>Nhà cung cấp</p>
          </a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">
            <i class="material-icons">category</i>
            <p>Loại sản phẩm</p>
          </a>
        </li>
        <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown" data-toggle="dropdown">
                <i class="material-icons">house</i>
                <span class="caret"></span>
                <p>Quản lý kho</p>
            </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Nhập hàng</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Xuất hàng</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Hàng tồn</a></li>
                </ul>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="#">
            <i class="material-icons">receipt</i>
            <p>Danh sách hóa đơn</p>
          </a>
        </li>
      </ul>
    </div>
@endsection('menu')