@extends('user/layout-user')

<!-- Main content  -->
<?php $i=1 ?>
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="container-fluid">
            <div class="card card-plain">
                <div class="card-header card-header-primary">
                    <h4 class="card-title" style="display:inline">Danh sách tài khoản</h4>
                    <a href="#" style="float:right;color:white;">
                    Thêm mới
                    <i class="material-icons">add_circle</i>
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table dt-responsive nowrap">
                            <thead class=" text-primary">
                            <th>STT</th>
                            <th>Creator</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Is_Public</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td>Dakota Rice</td>
                                <td>Niger</td>
                                <td>Oud-Turnhout</td>
                                <td class="text-primary">$36,738</td>
                                <td>
                                <a href="#">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="#">
                                    <i class="material-icons">delete</i>
                                </a>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td>Dakota Rice</td>
                                <td>Niger</td>
                                <td>Oud-Turnhout</td>
                                <td class="text-primary">$36,738</td>
                                <td>
                                <a href="#">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="#">
                                    <i class="material-icons">delete</i>
                                </a>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td>Dakota Rice</td>
                                <td>Niger</td>
                                <td>Oud-Turnhout</td>
                                <td class="text-primary">$36,738</td>
                                <td>
                                <a href="#">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="#">
                                    <i class="material-icons">delete</i>
                                </a>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td>Dakota Rice</td>
                                <td>Niger</td>
                                <td>Oud-Turnhout</td>
                                <td class="text-primary">$36,738</td>
                                <td>
                                <a href="#">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="#">
                                    <i class="material-icons">delete</i>
                                </a>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td>Dakota Rice</td>
                                <td>Niger</td>
                                <td>Oud-Turnhout</td>
                                <td class="text-primary">$36,738</td>
                                <td>
                                <a href="#">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="#">
                                    <i class="material-icons">delete</i>
                                </a>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td>Dakota Rice</td>
                                <td>Niger</td>
                                <td>Oud-Turnhout</td>
                                <td class="text-primary">$36,738</td>
                                <td>
                                <a href="#">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="#">
                                    <i class="material-icons">delete</i>
                                </a>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td>Dakota Rice</td>
                                <td>Niger</td>
                                <td>Oud-Turnhout</td>
                                <td class="text-primary">$36,738</td>
                                <td>
                                <a href="#">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="#">
                                    <i class="material-icons">delete</i>
                                </a>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td>Dakota Rice</td>
                                <td>Niger</td>
                                <td>Oud-Turnhout</td>
                                <td class="text-primary">$36,738</td>
                                <td>
                                <a href="#">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="#">
                                    <i class="material-icons">delete</i>
                                </a>
                                </td>
                            </tr>
                            </tbody>      
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')

@section('js')
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.js') }}"></script>

<!-- jquery pagination list  -->
<script type="text/javascript">
    $(document).ready(function(){$("#datatable").DataTable({
    language:{
        paginate:{
        previous:"<i class='mdi mdi-chevron-left'>",next:"<i class='mdi mdi-chevron-right'>"
                }
            },drawCallback:function(){
                $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
            }
    });
    });
</script>
@endsection('js')