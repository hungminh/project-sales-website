# Đồ Án Tốt Nghiệp

# FI Platform #

## Documentation ##
- [https://laravel.com/docs/5.7](https://laravel.com/docs/5.7)

## Installation
1. Clone project 
2. Run `composer install`.

3. Create file .env: 
```
cp .env.example .env

    php artisan key:generate
    php artisan jwt:secret
    php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
    
```
4. Update libarary:
```
composer update --lock
```